<?php
// Start of PHP execution counter
$start = microtime(true);

// Include main app file
require_once(__DIR__ . '/app/index.php');

// Get PHP script execution time in ms
echo round((microtime(true) - $start) * 1000) . "ms";

// Testing
d($url_array);