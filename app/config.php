<?php
// var_dump() nesting limit - unlimited levels
ini_set("xdebug.var_display_max_depth", -1);
ini_set("xdebug.var_display_max_children", -1);
ini_set("xdebug.var_display_max_data", -1);

// Database connection settings
$db_settings = array(
	'HOST' => '',
	'USER' => '',
	'PASS' => '',
	'DB'   => ''
);