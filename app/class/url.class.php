<?php
class Url
{
	// Main array with stored values about URL (values storing is in code below)
	private $url_array;

	// Array with parset URL
	private $request_uri;

	// Imploded array of parsed URL
	private $request_uri_imploded;

	// Size of parsed request uri
	private $request_uri_size;

	// Popped array of request uri
	private $request_uri_pop;

	// Imploded popped array of request uri
	private $request_uri_pop_imploded;

	// All tables that are allowed to be searched from
	private $allowed_search_types = array('languages', 'sections', 'pages');

	// All available languages, sections, pages
	private $db_records;

	// Language ID
	private $language_id;

	// Default language ID
	private $default_language_id;

	public function __construct()
	{
		// Main array with stored values about URL (values storing is in code below)
		$this->url_array = array('host' => null, 'language' => null, 'section' => null, 'page' => null);

		// Get full url address from address bar without hostname (starting with slash)
		$this->request_uri      = array_values(array_filter(explode('/', $_SERVER['REQUEST_URI'])));
		$this->request_uri_size = count($this->request_uri);

		// Fill in values to $url_array
		$this->url_array['host']['protocol'] = (!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] !== 'off' ? 'https' : 'http');
		$this->url_array['host']['name']     = $_SERVER['SERVER_NAME'];
		$this->url_array['host']['ip']       = $_SERVER['SERVER_ADDR'];
		$this->url_array['host']['port']     = $_SERVER['SERVER_PORT'];

		// Load all languages, sections, pages to array
		$this->getAllRecordsFor('languages');
		$this->getAllRecordsFor('sections');
		$this->getAllRecordsFor('pages');

		// Set default values
		$this->setDefaultLanguage();

		// Search for languages
		$this->searchUrlFor('languages');

		// Load language id to variable for in-class work
		$this->language_id = (isset($this->url_array['language']['url']['id']) ? $this->url_array['language']['url']['id'] : $this->url_array['language']['default']['id']);

		// Search for sections
		$this->searchUrlFor('sections');

		// Check if found page exists for current section
		$this->checkPageForSection($this->url_array['section']['id']);
	}

	public function setDefaultLanguage()
	{
		GLOBAL $db;

		// Find default language by searching inside settings
		$sql   = "SELECT value FROM settings WHERE xml_name LIKE 'vychozi-jazyk';";
		$query = $db->prepare($sql);
		$query->execute();
		$default_lang_id = intval($query->fetchAll(PDO::FETCH_ASSOC));

		// Set default language from settings
		$sql   = "SELECT * FROM languages WHERE id = $default_lang_id;";
		$query = $db->prepare($sql);
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_ASSOC);

		$this->url_array['language']['default'] = array_shift($result);
		$this->default_language_id = $this->url_array['language']['default']['id'];
	}

	public function popArray($array = array())
	{
		if (empty($array)) {
			return null;
		} else {
			array_pop($array);
			return $array;
		}
	}

	public function shiftArray($array = array())
	{
		if (empty($array)) {
			return null;
		} else {
			array_shift($array);
			return $array;
		}
	}

	public function getAllRecordsFor($table = null)
	{
		if ($table === null) {
			die("You have to specify the table to get from DB!");
		} elseif (!in_array($table, $this->allowed_search_types)) {
			die("Specified table $table is not allowed to be searched from!");
		} else {
			GLOBAL $db;

			$table = htmlspecialchars($table);

			if ($table === 'languages') {
				$sql = "SELECT * FROM languages;";
			} else {
				if ($table === 'sections')
					$search_col = 'section_id';
				elseif ($table === 'pages')
					$search_col = 'page_id';
				elseif ($table === 'products')
					$search_col = 'product_id';

				$sql = "SELECT urls.$search_col, urls.language_id, urls.url, $table.* FROM urls INNER JOIN $table ON urls.$search_col = $table.id WHERE urls.$search_col IS NOT NULL;";
			}

			$table = substr($table, 0, -1);

			$query = $db->prepare($sql);
			$query->execute();
			$results = $query->fetchAll(PDO::FETCH_ASSOC);

			$this->db_records[$table] = $results;
		}
	}

	public function emptyStringToNULL($string = null)
	{
		if ($string === null) {
			die("emptyStringToNULL(): You have to specify string you want to convert!");
		} else {
			if (strlen(trim($string)) === 0) {
				return null;
			} else {
				return $string;
			}
		}
	}

	public function searchUrlFor($search_type = null)
	{
		GLOBAL $db;

		$search_type = htmlspecialchars($search_type);

		if (in_array($search_type, $this->allowed_search_types)) {
			$this->request_uri_pop  = $this->request_uri;
			$this->request_uri_size = count($this->request_uri);

			$search_type = substr($search_type, 0, -1);

			for ($i = 0; $i <= $this->request_uri_size; $i++) {
				if(!is_null($this->request_uri_pop)) {
					$this->request_uri_pop_imploded = implode('/', $this->request_uri_pop);
					$this->request_uri_pop          = $this->popArray($this->request_uri_pop);

					$this->request_uri_pop_imploded = $this->emptyStringToNULL($this->request_uri_pop_imploded);
				}

				foreach ($this->db_records[$search_type] as $key => $value) {
					if ($value['url'] === $this->request_uri_pop_imploded) {
						if ($search_type == 'language') {
							$this->url_array[$search_type]['url'] = $value;
						} else {
							$this->url_array[$search_type] = $value;
						}

						if (!is_null($value['url'])) {
							$this->request_uri      = $this->shiftArray($this->request_uri);
							$this->request_uri_size = count($this->request_uri);
						}

						break;
					}
				}
			}
		} else {
			die("$search_type is not valid value to search!");
		}
	}

	public function checkPageForSection($section_id = null)
	{
		GLOBAL $db;

		if(!is_null($section_id)) {
			$this->request_uri_imploded = implode('/', $this->request_uri);
			$this->request_uri_imploded = $this->emptyStringToNULL($this->request_uri_imploded);

			foreach ($this->db_records['page'] as $key => $value) {
				$value_url         = $value['url'];
				$value_section_id  = $value['section_id'];
				$value_language_id = $value['language_id'];

				if (($value_url === $this->request_uri_imploded) && ($value_section_id === $section_id)) {
					if ($value_language_id === $this->language_id)
						$this->url_array['page'] = $value;
					elseif ($value_language_id === $this->default_language_id && is_null($value_url))
						$this->url_array['page'] = $value;
				}
			}
		} else {
			die("checkPageForSection(): You have to set id of section to find the page in!");
		}
	}

	public function getUrlArray()
	{
		return $this->url_array;
	}
}