<?php
// Set UTF-8 to the document
header('Content-Type: text/html; charset=utf-8');

// Loads-up configuration
require_once __DIR__ . '/config.php';

// Default definitions
DEFINE('BASE', __DIR__ . '/');

// Autoload class when it's called out
function __autoload($class_name) {
	$class_name = strToLower($class_name);
	include BASE . 'class/' . $class_name . '.class.php';
}

// Load Kint debugger
include BASE . 'libs/kint-master/Kint.class.php';

// Connect to DB
try {
	$db = new PDO('mysql:host=' . $db_settings['HOST'] . ';dbname=' . $db_settings['DB'] . '', $db_settings['USER'], $db_settings['PASS'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
} catch (PDOException $error) {
	die($error->getMessage());
}

// Fill in URL array
$url       = new Url();
$url_array = $url->getUrlArray();